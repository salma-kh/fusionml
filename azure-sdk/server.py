from flask import Flask, request, jsonify
from azure_service import AzureService
import os

app = Flask(__name__)
azureService = AzureService (workspace_name = 'hello', subscription_id = 'fd6066b9-a7ea-4977-8940-34a72b93c567', resource_group = 'MyGroup', experiment_name = 'compute1')
os.makedirs("scripts", exist_ok=True)

@app.route('/scripts/run', methods=['POST'])
def run_script():
    script = request.files['file']
    script_path = "scripts/" + script.filename
    script.save(script_path)
    dependencies_str = request.form['dependencies']
    dependencies_list = dependencies_str.split(';')
    run_id = azureService.run_script(script_path, dependencies_list)
    return jsonify(id = run_id)

@app.route('/scripts/<id>/status', methods=['GET'])
def get_script_status(id):
    status = azureService.script_status(id)
    return jsonify(status = status)

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000)