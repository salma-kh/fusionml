from azureml.core import Workspace
from azureml.core.experiment import Experiment
from azureml.core import ScriptRunConfig
from azureml.core.runconfig import RunConfiguration
from azureml.core.compute import AmlCompute
from azureml.core.conda_dependencies import CondaDependencies
from azureml.core.model import Model
import os

class AzureService:
    def __init__(self, workspace_name, subscription_id, resource_group, experiment_name):
        self.ws = Workspace.get(name = workspace_name, subscription_id = subscription_id, resource_group = resource_group)
        self.experiment = Experiment(workspace = self.ws, name = experiment_name)

    def run_script(self, script, denpendencies_list):
        compute_config = self.build_compute_config(denpendencies_list) 
        script_run_config = ScriptRunConfig(source_directory=os.getcwd(), script=script, run_config=compute_config)
        run = self.experiment.submit(config=script_run_config)
        return run.id

    def build_compute_config(self, denpendencies_list):
        compute_config = RunConfiguration()
        compute_config.target = 'amlcompute'
        compute_config.amlcompute.vm_size = 'STANDARD_D1_V2'
        dependencies = CondaDependencies()
        for dependency in denpendencies_list:
                dependencies.add_pip_package(dependency)
        compute_config.environment.python.conda_dependencies = dependencies
        return compute_config

    def script_status(self, id):
        list_runs = self.experiment.get_runs()
        run = next(r for r in list_runs if r.id == id)
        return run.status