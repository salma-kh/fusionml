package com.ml.service;

import java.util.ArrayList;
import java.util.List;

public class Configuration {
    private List<String> dependencies = new ArrayList<>();
    private String dataVersion;

    public List<String> getDependencies() {
        return dependencies;
    }

    public String getDataVersion() {
        return dataVersion;
    }

    public void setDataVersion(String dataVersion) {
        this.dataVersion = dataVersion;
    }

    public void setDependencies(List<String> dependencies) {
        this.dependencies = dependencies;
    }
}
