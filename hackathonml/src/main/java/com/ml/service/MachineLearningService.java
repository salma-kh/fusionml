package com.ml.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.net.URI;
import java.util.Collections;

@Service
public class MachineLearningService {
    @Value("${api.python}'")
    private String pythonUrl;


    public String uploadModel(MultipartFile file, String configuration) {
        try {
            Configuration conf = new Configuration();
            ObjectMapper objectMapper = new ObjectMapper();
            if (!StringUtils.isEmpty(configuration)) {
                conf = objectMapper.readValue(configuration, Configuration.class);
            }
            HttpHeaders headers = new HttpHeaders();
            // set `content-type` header
            headers.setContentType(MediaType.MULTIPART_FORM_DATA);
            // set `accept` header
            headers.setAccept(Collections.singletonList(MediaType.MULTIPART_FORM_DATA));

            final RestTemplate restTemplate = new RestTemplate();
            MultiValueMap<String, Object> body
                    = new LinkedMultiValueMap<>();
            File tempFile = File.createTempFile(file.getName(),"py");
            file.transferTo(tempFile);
            FileSystemResource fileSystemResource = new FileSystemResource(tempFile);

            body.add("file", fileSystemResource);
            //Map<String, Object> maps = new HashMap<>();
            //maps.put("file", file);
          body.put("dependencies", Collections.singletonList(conf.getDependencies()));
            HttpEntity<MultiValueMap<String, Object>> entity =new HttpEntity<>(body,headers);
            ResponseEntity<String> response = restTemplate.postForEntity(URI.create("http://localhost:5000/scripts/run"), entity, String.class);
            return response.getBody();
        } catch (Exception ex) {
            throw new RuntimeException("cannont parse configuration", ex);
        }
    }

    public ResponseEntity<String> getStatus(final String id) {
             RestTemplate restTemplate =new RestTemplate();
            return restTemplate.getForEntity(URI.create("http://localhost:5000/scripts/"+id+"/status"),String.class);
    }
}
