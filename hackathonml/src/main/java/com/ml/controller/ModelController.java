package com.ml.controller;


import com.api.fusion.ml.ModelsApi;
import com.ml.service.MachineLearningService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;

import java.net.URI;


@RestController
@RequestMapping("/api/ml/v1/")
public class ModelController implements ModelsApi {

    @Autowired
    private MachineLearningService machineLearningService;


    @Override
    public ResponseEntity<String> uploadModel(MultipartFile experimentFile, String configuration) {
        return ResponseEntity.ok(machineLearningService.uploadModel(experimentFile, configuration));
    }

    @Override
    public ResponseEntity<String> getStatus(String id) {
        return machineLearningService.getStatus(id);
    }
}
